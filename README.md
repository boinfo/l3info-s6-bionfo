# readme l3info-s6-bionfo

# Git pour l'UE PE boinfo en L3 info

## Informations générales sur l'UE

- 3 ECTS
- 23h TD
- le vendredi après-midi à 13h
- en salle TP

## Contenu

Contenu ouvert pour ne pas avoir que des étudiants qui veulent aller en MISO qui choisissent cette option. 
Avoir des problématiques en lien avec des connaissances bio pour les apporter au fur et à mesure

- accès à une base de données via une interface web
- récupération des données selon certains critères
- écriture de petits algo pour analyser ces données
- apport des notions de bio au fur et à mesure 

## Idée de fil rouge : prédiction de site de fixation de facteurs de transcription

Description :

    UE en mode projet : on a un objectif à atteindre et on apprend les notions (info, bio, bioinfo) nécessaires au fur et à mesure

    récupération de séquences amont d'une liste de gènes

    recherche de motifs conservés

    langage : Python
    utilisation de bibliothèques
    objectif : avoir développé un outil qui peut servir à un.e biologiste
    Pas besoin de connaissances préalables en biologie
    
MCC :
    - QCM sur les notions de bio
    - rendu/recette intermédiaire
    - projet avec recette finale à l'oral

Todo : trouver un exemple d'accroche

Jolie image (libre) sur les facteurs de transcription : https://commons.wikimedia.org/wiki/File:Transcription_Factors-fr.svg 

### Semainer de l'UE (6, non 5 séances)

- 5 séances de 4h de développement du projet
- +1 séance de 3h de soutenances ou intégré ?

#### Semaine 1 (4h)

**Objectif** : 
- introduire les notions de bio de base pour le projet
- manipuler les données

##### Cours (0h45)
- introduire le dogme central, 
- parler des différents objets, de la structure des gènes eucaryotes,
- puis se focaliser sur comment se passe la transcription,
  - vidéo ? https://www.youtube.com/watch?v=VQJKDgpRcnI
- parler de la reconnaissance de sites de fixation,
 
##### TP (3h15)
- Partie 1 (1h30)
  - manipulations sur le NCBI : 
    - trouver une séquence
    - voir ses annotations
    - afficher différents formats 
    - faire faire des requêtes
    - montrer les liens entre les différentes bases de données pour préparer la recherche de la séquence promotrice
  - manipulations sur JASPAR (Transfac est payant) http://jaspar.genereg.net/
- Partie 2 (1h30)
  - API du NCBI https://www.ncbi.nlm.nih.gov/home/develop/api/
    https://www.ncbi.nlm.nih.gov/books/NBK25500/
    - requête esearch 
      https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch/esearch.fcgi?db=nucleotide&term=NM_007389
    - requête efetch 
      https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch/efectch.fcgi?db=nucleotide&id=NM_007389&rettype=fasta&retmode=text
      https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch/efectch.fcgi?db=nucleotide&id=NM_007389&rettype=gb&retmode=text
  - mise en place des dépôts des binomes
  - télécharger et organiser les données

#### Semaine 2 (4h)

**Objectif** : 
- disposer du code permettant de calculer télécharger la partie amont d'un gène étant donné son identifiant
- rechercher les occurrences d'une PWM dans une séquence

##### Cours (0h45)
- revenir sur la modélisation des sites de fixation : PWM, PSSM
- introduire le problème de la recherche de sites dans des gènes co-régulés
    http://212.48.67.52/cgi-bin/dGCR.cgi
- biopython

##### TP (3h15)
- une partie du tutoriel biopython 
  - représentation des séquences
  - lecture d'une séquence à partir d'un fichier
  - extraire des informations
- mise en oeuvre du téléchargement de séquences en amont d'un gène
  - s'intéresser à la doc Efecth et consort de biopython
- si le temps, début du calcul de score pour une PWM

#### Semaine 3 (4h)

**Objectif** : 
- finaliser le code permettant la recherche d'occurrences

##### Cours (0h30)
- comment trouver les co-occurrences ? fenêtre glissante, calcul de score

##### TP (3h30)
- recherche des occurrences d'une PWM
- recherche des occurrences et stockage d'une PWM pour un ensemble de séquences
- débuter le travail sur le calcul de scoire d'une fenêtre glissante

##### Rendu intermédiaire

Un script qui prend en entrée une PWM, un score seuil, un ID de gène, la longueur du promoteur et affcihe les positions d'occurrence

#### Semaine 4 (4h)

**Objectif** : 
- tout mettre ensemble
- si des étudiants sont en avance, les faire travailler à un export JSON permettant d'afficher les résultats sur une page web

##### Cours (pas de cours)

##### TP (4h)

#### Semaine 5 (4h)

**Objectif** : soutenances ?

##### Cours
##### TP





### Tentative de découpage en jalons du projet (JSV)

**Objectif** : étant donné un ensemble de gènes co-régulés, trouver les sites de fixation qui régulent cet ensemble

Etapes de l'analyse

    - extraire les séquences promotrices des gènes

    - rechercher les occurrences de plusieurs sites de fixation de facteurs de transcription (sous la forme d'une PWM)

    - pour chaque matrice calculer un score de fenêtre glissante dans le but d'identifier des clusters


#### Jalon 1 : étant donné une liste de gènes (identifiants) extraire les séquences en amont du gène

**Objectif** : extraire des régions promotrices (alignées à droite sur le TSS)

=> est-ce faisable simplement avec Biopython ? 
https://www.biostars.org/p/406554/
https://zhiganglu.com/post/py-upstream-sequence/
On a du code dans Vidjil qui extrait les régions upstream ou downstream de gènes dont on a les identifiants, en interrogeant l'API du NCBI (https://gitlab.inria.fr/vidjil/vidjil/-/blob/a90864e5/germline/ncbi.py#L51 ), mais attention aux restrictions cela dit (https://www.ncbi.nlm.nih.gov/books/NBK25497/#chapter2.Coming_in_December_2018_API_Key )

**Cours (notions à introduire donc)**
- introduire le dogme central, 
- puis se focaliser sur comment cela se passe la transcription
- parler de la reconnaissance de sites spécifiques
à faire en cours : introduire toute la biologie :)

**Pratique**
- utilisation d'une banque pour aller chercher une séquence
- voir qu'il y a des annotations (le TSS par exemple)
- voir comment à la main on peut aller chercher un promoteur
- faire une recherche de sites sur ce promoteur

#### Jalon 2 : afficher les occurrences des sites pour l'ensemble des régions promotrices

**Objectif** : trouver les occurrence de sites de fixation dans des promoteurs
- nécessite d'avoir modélisé une PWM et l'algorithme de recherche (à faire en cours : alignement multiple, construction d'une matrice de fréquence, score d'une matrice vs. une séquence, matrice poids-position introduction de pseudo count, background modèle, p-value)
- modéliser le stockage des résultats pour chaque séquence
- téléchargement des matrices JASPAR (expliquer les LOGO affichés sur la base de données)
- visualiser simplement avec une ligne par région et un marqueur à chaque position d'occurrence
    
#### Jalon 3 : calculer un score avec une fenêtre glissante

**Objectif** : extraire les régions denses d'un site de fixation donné

à faire en cours : fenêtre glissante, calcul du score de la fenêtre glissante

#### Jalon 4 : présentation des résultats

**Objectif** : fournir un outil permettant d'identifier 

à faire en cours : discuter de l'ergonomie de l'application


=======
Portail de l'option bioinfo en L3.

Ce dépôt est public, le contenu est disponible sur [le dépôt (privé)](https://gitlab-fil.univ-lille.fr/boinfo/l3info-s6-contenu)

