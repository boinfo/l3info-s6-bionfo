<!-- exemple de description des modalités d'évaluation d'une UE -->
<!-- la classe NOTE attribue une couleur -->
<!-- la classe FORMULE pour les tags  -->

<h3> Modalités d'évaluation des connaissances</h3>
<p>
Trois notes seront attribuées à chaque étudiant durant le semestre :
</p>

<ul>
  <li> <span class="NOTE">Bio</span> : une note sur 20 de questions
courtes type QCM sur les notions de biologie découvertes pendant le semestre.</li> 
  <li> <span class="NOTE">Ri</span> : une note sur 20 portant sur
un rendu intermédiaire des développements réalisés.</li> 
  <li> <span class="NOTE">Proj</span> : une note sur 20 portant sur le rendu final, avec une présentation orale du travail.</li> 
</ul>

<p>
La note finale sur 20 (<span class="NOTE">N</span>) est calculée comme suit :
</p>


<p class="FORMULE">
 N = 25% Bio + 25% Ri + 50% Proj
</p>

<!--
<p>La session de rattrapage remplace l'examen, les
notes de Bio et de Ri sont conservées.
</p>
--> 

<p>
L'unité acquise apporte 3 ECTS.
</p>
