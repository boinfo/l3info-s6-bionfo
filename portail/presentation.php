<!-- Le nom de l'UE -->
<div class="UE">Bioinformatique</div>
<div class="bcc">BCC 7 : Approfondir ses connaissances disciplinaires</div>

<h3> Pré-requis </h3>
<ul>
  <li>Savoir programmer en Python.</li>
  <li>Il n'est pas nécessaire d'avoir des connaissances en biologie.
      Les notions nécessaires seront découvertes pendant l'UE.</li>
</ul>

<!-- Volume horaire et organisation de l'UE C, CTD, TD, TP -->
<h3> Organisation  </h3>

<p>
Cette option se déroule au S6 de la Licence informatique.
Une grande part sera donnée à la pratique sur ordinateur,
en mode projet.
</p>

<p>
Volume horaire : 24h
</p>

<p>
Cette UE permet de découvrir la bioinformatique : appliquer la science informatique à un autre domaine scientifique, comprendre les données manipulées, appréhender l'apport de l'informatique à la biologie. 
</p>

<p>
Si vous êtes intéressé·e·s 
pour aller plus loin, vous pouvez candidater au Master bioinformatique,
parcours <a href="https://www.fil.univ-lille1.fr/portail/index.php?dipl=M1MISO">MISO</a>
(Méthodes informatiques et statistiques pour les omics), et jeter un oeil aux métiers sur le site de la <a href="https://www.sfbi.fr/metiers-formations">
Société française de bioinformatique (SFBI)</a></p>


<h3> Crédits </h3>

<strong>3 ECTS</strong>


<h3> Intervenants </h3>

<p>
Jean-Stéphane Varre, Bât. M3 extension, bureau 206, e-mail : <tt>jean-stephane.varre [AT] univ-lille.fr </tt> <br/>
Mikaël Salson, Bât. M3 extensiion, bureau 216, e-mail : <tt>mikael.salson [AT] univ-lille.fr </tt><br/>
Olivier Pluquet, e-mail : <tt>olivier.pluquet2 [AT] univ-lille.fr </tt>
</p>
