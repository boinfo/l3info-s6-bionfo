<!-- objectifs de l'UE -->
<h3> Objectifs </h3>

Cette option de bioinformatique a deux objectifs
<ul>
   <li>faire découvrir l'informatique appliquée à la biologie et ses spécificités,</li>
   <li>mener un projet en respectant les bonnes
   pratiques de programmation et exploitant des bibliothèques existantes.
   </li>
</ul>

<!-- le contenu -->
<h3> Contenu </h3>

Le langage de programmation utilisé sera Python qui a l'avantage
de proposer de nombreuses bibliothèques scientifiques. 

Les éléments de programmation que vous mettrez en œuvre 
et notions que vous allez découvrir :

<ul>
   <li>accès à une base de données via une interface web,</li>
   <li>utilisation de bibliothèques (modules) scientifiques,</li>
   <li>algorithmes pour la comparaison de données biologiques,</li>
   <li>écriture de tests unitaires (pratique indispensable à une programmation de qualité),</li>
   <li>notions de biologie découvertes au fur et à mesure
   des séances.</li>
</ul>

<h3>Fil rouge</h3>

Le projet à mener sera la programmation d'un outil pouvant servir 
à l'analyse de données biologiques. Pour être plus concret,
les cellules interagissent avec leur environnement et entre elles.
Des signaux extérieurs induisent la modification de la synthèse 
de protéines (souvent des enzymes), ce qui modifie le comportement 
des cellules. L'outil développé aidera à déterminer quelles protéines
seront influencées par un signal donné.


<!-- bibliographie accompagnant l'UE
<h3> Liens utiles </h3>
 -->
