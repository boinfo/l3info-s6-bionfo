# TP1 - Appréhender des séquences biologiques

Plusieurs banques de données internationales centralisent les séquences
biologiques obtenues par des scientifiques autour du monde.

Nous allons ici nous concentrer sur le [NCBI](https://www.ncbi.nlm.nih.gov/)
(National Center for Biology Information), le centre de bioinformatique des
États-Unis d'Amérique. Ce centre pointe vers de nombreuses ressources : des
séquences ADN ou ARN, des séquences protéiques, des articles scientifiques,
des livres…

Ici nous allons aller sur [le site qui correspond aux séquences
nucléotidiques](https://www.ncbi.nlm.nih.gov/nuccore) (ADN ou ARN).

## Étudier une séquence ARN

Dans le champ de recherche, entrer `CHRNA1`. CHRNA1, est [le nom d'un gène](https://en.wikipedia.org/wiki/CHRNA1) qui code pour une protéine réceptrice de l'acétylcholine (un neurotransmetteur).

Une fois la recherche validée, vous devez obtenir quelques milliers de résultats. Ce qui est beaucoup. Nous allons les filtrer pour obtenir ce qui nous intéresse.

### Filtrer les résultats

1. Parmi les organismes disponibles, sélectionner la souris (*Mus musculus*), sur la droite de la page, dans *Top organisms*, cliquez sur *More…* pour voir plus d'organismes. Constatez que cela modifie votre champ de recherche pour y ajouter cette contrainte.
2. Nous voulons nous intéresser ici à un ARN messager (ARNm, ou *mRNA* en anglais) du gène CHRNA1. Ne conservez que les ARNm en choisissant l'entrée correspondante (à gauche de la page).
3. Enfin, ne conservez que les résultats provenant de RefSeq (à gauche de la page), qui ne contient que des données vérifiées sans doublon.

Il ne reste plus qu'un seul résultat, c'est celui qui nous intéresse.

Nous n'arrivons pas toujours à un seul résultat avec cette démarche, il peut par exemple y avoir plusieurs ARNm pour un même gène, ou alors le nom du gène peut être présent sans que la molécule vienne de ce gène, dans ce cas il aurait fallu raffiner la recherche en précisant que `CHRNA1` était le nom du gène, en notant `CHRNA1[gene name]`.
### Comprendre une entrée

Une entrée possède de nombreuses informations la décrivant mais également
détaillant comment la séquence a été identifiée. Le format utilisé pour cette
description est un format texte appelé *GenBank* ([documentation](https://www.ncbi.nlm.nih.gov/genbank/samplerecord/)).

Sur la première ligne, commençant par `LOCUS` on a notamment l'identifiant de cet ARN messager (commençant par `NM_`), sa taille en nucléotide, le type de molécule.

Un peu plus bas, la ligne commençant par `ORGANISM` confirme qu'il s'agit bien
d'une séquence identifiée chez des souris. Ensuite de nombreuses lignes
correspondent aux références des articles scientifiques ayant participé à la
mise en évidence de cet ARN messager et de ses caractéristiques.

Descendez jusqu'à la ligne qui commence par `FEATURES`.
Un certain nombre d'informations sont visibles.
Identifiez : 
1. Le chromosome où le gène codant cet ARNm est présent
2. Un autre nom pour le gène CHRNA1
3. L'identifiant numérique de ce gène
4. Le nombre d'exons

Enfin la séquence ADN de l'ARNm est présente à la fin du fichier.  La séquence
est écrite sous forme de blocs de 10 nucléotides séparées par une espace, mais
ce format n'est pas le plus simple pour la manipulation des séquences.
### Formats de données

Nous allons visualiser cette entrée sous un autre format.
Revenez tout en haut de la page.
Cliquez à l'endroit où se trouve un lien *GenBank* (au dessus du nom de l'ARNm) et choisissez le format *Fasta (text).

S'affiche alors la séquence de l'ARNm au format FASTA, un format texte simple
dans lequel les lignes commençant par `>` décrivent la séquence écrite sur les
lignes suivantes (les sauts de ligne dans la séquence n'ont pas de
signification particulière). Ce format est très utilisé en bioinformatique
pour travailler sur les séquences.

Revenez en arrière et sélectionnez un autre format, plus visuel : *Graphics*.
Est-ce que cela confirme bien le nombre d'exons précédemment identifié ?

À quelle position commence et se termine la séquence de la protéine (sur fond
rouge, dont le nom commence par `NP_`) ?
Pourquoi la protéine ne recouvre pa tout le gène ?


### Au-delà du transcrit
Pour que ce gène soit exprimé, ce qui nous intéresse c'est le promoteur du
gène qui est en amont de celui-ci.  Or pour identifier cette région amont, il
faut s'intéresser à la position de ce gène sur le chromosome sur lequel il se
trouve.

Dans l'entrée GenBank nous avons identifié le chromosome sur lequel ce gène se
trouve. Pour autant nous ne connaissons pas sa position sur le chromosome.

Pour cela nous allons utiliser les liens que fait le NCBI entre ces
différentes banques de données.  Nous sommes actuellement sur la banque de
données nucléotides, sur une entrée d'un ARNm, et nous allons nous rendre sur
l'entrée qui correspond à ce gène dans la banque *Gene*.

Pour cela cliquer sur *Gene* dans *Related information* dans le bandeau de droite.

Sur cette page, trouvez l'identifiant du gène, qui doit être le même que celui relevé précédemment dans les informations de l'entrée GenBank.

Ensuite vous trouverez le nom du chromosome, son numéro d'accession
(commençant par `NC_`) et les positions sur celui-ci.

Si nous voulons étudier les positions en amont du gène, quelles positions
devons-nous étudier ? (attention au brin sur lequel le gène se trouve)

## Facteurs de transcription

XXX


## Utiliser l'API

Nous avons fait tout cela manuellement, mais il est nécessaire d'automatiser
cela afin de pouvoir traiter les données automatiquement.

Pour cela le NCBI offre [une API](https://www.ncbi.nlm.nih.gov/books/NBK25501/).

Cette API peut être utilisée en accédant directement à certaines URL ou, via des packages dans certains langages de programmation.

- [esearch](https://www.ncbi.nlm.nih.gov/books/NBK25500/#chapter1.Searching_a_Database) : permet de chercher certains termes dans une base de données (p. ex. https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch/esearch.fcgi?db=MY_DB&term=MY_TERM)
- [efetch](https://www.ncbi.nlm.nih.gov/books/NBK25500/#chapter1.Downloading_Full_Records) : permet de récupérer une entrée précise dans une base de données (p. ex. https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch/efectch.fcgi?db=MY_DB&id=MY_ACCESSION&rettype=MY_TYPE&retmode=text)
- [elink](https://www.ncbi.nlm.nih.gov/books/NBK25500/#chapter1.Finding_Related_Data_Through_En) : Faire un lien entre une entrée dans une base de données et l'entrée correspondante dans une autre base de données (p.ex. https://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink/elink.fcgi?dbfrom=MY_DB_FROM&db=MY_DB&id=MY_ACCESSION)

Renouvelez ce que nous avons fait précédemment mais en utilisant cette API : 

Effectuez la recherche avec `esearch` (sans les filtres), sur la base de données `nucleotide` (attention à bien remplacer `MY_DB` et `MY_TERM` dans l'URL donnée en exemple). 

Quel est le format obtenu ?
Combien de résultats y a-t-il au total (pas seulement ceux affichés) ? 

Afin d'appliquer les mêmes filtres que précédemment il faudra indiquer comme terme de recherche `Chrna1 AND biomol_mrna[PROP] AND refseq[filter] AND "Mus musculus"[Organism]`.
Quel est l'identifiant du résultat obtenu ?


Utilisez maintenant `efetch` pour récupérer l'entrée correspondante dans cette banque de données. Récupérez l'entrée au format GenBank (`gb`) puis FASTA (`fasta`) (ce qui remplace `MY_TYPE`, qui est le type de format de données).

Réessayez en remplaçant l'identifiant par le numéro d'accession de la molécule (commençant par `NM_`). Obtenez-vos la même chose ?

Enfin utilisez `elink` afin de passer de la banque de données `nucleotide` à la banque de données `gene`, comme précédemment sur le site web. Cela nous permettra de connaître l'identifiant du gène.
<!-- https://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink/elink.fcgi?dbfrom=nucleotide&db=gene&id=NM_007389.5 -->

Avec `efetch` récupérez les informations, au format GenBank, pour ce gène-là (banque de données `gene`).
Cela nous (re)donne le numéro d'accession du chromosome et les positions du gène sur le chromosome.
<!-- https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch/efetch.cgi?id=11435&db=gene&rettype=gb&retmode=text -->

Utilisez à nouveau `efetch` pour récupérer la séquence en amont de ce gène (pour préciser les positions, il faut utiliser `seq_start` et `seq_stop`, `seq_start` étant supérieur à `seq_stop` lorsqu'on est sur le brin anti-sens).
<!-- https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch/efectch.fcgi?db=nucleotide&id=NC_000068.8&rettype=fasta&retmode=text&seq_start=73410782&seq_stop=73410683 -->
